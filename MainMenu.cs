﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*#########################################################################################################################
#                                                                                                                         #
#  DUNGEONS AND DRAGONS CHARACTER GENERATOR                                                                               #
#                                                                                                                         #
#  WRITTEN BY                                                                                                             #
#  - CAMERON WATT        - s3589163  ( CODING )                                                                           #
#  - CAMILLA MARIA MOSES - s3590513  ( UNITY INTERFACE )                                                                  #
#                                                                                                                         #
#  THIS FILE'S SOLE PURPOSE IS TO DISPLAY THE MAIN MENU, AND ANY EXTRA FUNCTIONS                                          #
#                                                                                                                         #
#########################################################################################################################*/

namespace CCS_D_D_Character_Generator
{
    class MainMenu
    {
        // CONSTRUCTOR FOR THE Program CLASS WHEN CALLED, RUNS THE MENU
        public MainMenu()
        {
            // RUNS THE MENU SECTION OF THE PROGRAM
            Menu();
        }

        private void Menu()
        {
            // STORE THE USERS MENU CHOICE
            String userInput = "";

            while (userInput != "Q")
            {
                userInput = ""; // RESET USERINPUT AFTER PREVIOUS LOOP

                // PROGRAM INTRODUCTION
                Console.WriteLine("");
                Console.WriteLine(" ### The Dungeons and Dragons Character Sheet Generator ###");
                Console.WriteLine("");
                Console.WriteLine("Please enter a selection");
                Console.WriteLine("");
                Console.WriteLine("Press A to Show more information about the Program");
                Console.WriteLine("Press G to Generate a New Character");
                Console.WriteLine("Press X to Exit");

                userInput = Console.ReadLine().ToUpper();  // CONVERT RESPONSE TO UPPERCASE // reads an ENTIRE STRING

                // userInput = Console.ReadKey().ToString().ToUpper();  useless won't work
                //Console.ReadLine(); // CLEAR ANY EXTRA INPUT FROM THE BUFFER

                switch (userInput)
                {
                    case "A":
                        {
                            Console.WriteLine();
                            Console.WriteLine("Dungeons and Dragons - Character Generator");
                            Console.WriteLine("Written by Cameron \t- s3589163");
                            Console.WriteLine("\t   Camila \t- s3590513");
                            Console.WriteLine();
                            break;
                        }
                    case "G":
                        {
                            Character newCharacter = new Character();
                            break;
                        }
                    case "X":
                        {
                            Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please Enter a VALID Selection");
                            break;
                        }
                } // CLOSE switch (userInput)

            }  // END OF WHILE LOOP

        } // CLOSE String mainMenu()

    } // CLOSE class Generator

}  // CLOSE namespace CCS_D_D_Character_Generator
