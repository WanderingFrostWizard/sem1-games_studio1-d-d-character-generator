﻿/*###############################################################################################################################################
#                                                                                                                                               #
#   DUNGEONS AND DRAGONS CHARACTER GENERATOR - WITH UNITY INTERFACE   -   CURRENT VERSION - 10    26 MAY 2016                                   #
#                                                                                                                                               #
#   MOVE CAMERA / ZOOM SCRIPTS - COMPILED BY CAMERON WATT - S3589163                                                                            #
#                                                                                                                                               #
#   MOST OF THE CODE IN THIS SCRIPT IS NOT MY OWN, IT WAS FOUND ON THE FOLLOWING WEBSITES:                                                      #
#      http://forum.unity3d.com/threads/moving-main-camera-with-mouse.119525/                                                                   #
#      http://forum.unity3d.com/threads/simple-camera-zooming-script-for-absolute-newbie.72460/                                                 #
#                                                                                                                                               #
#   I SIMPLY COMPILED THE CODES TOGETHER, AND ADDED MY OWN ADDITIONS TO ALLOW THE USER TO MOVE THE CAMERA, BUT ONLY TO A SET DISTANCE FROM      #
#        THE CENTRE OF THE PAGE                                                                                                                 #
#                                                                                                                                               #
###############################################################################################################################################*/ 

#pragma strict

// Move Camera Script
 
var horizontalSpeed : float = 40.0;
var verticalSpeed : float = 40.0;

var horizontalPosition = 0;
var verticalPosition = 0;

var allowedRadiusFromCentre = 500;  // ALLOWS THE BEST OPPORTUNITY FOR THE USER TO ZOOM IN NEAR THE EDGES OF THE CHARACTER SHEET
 
function Update () 
{
    // THIS FUNCTION WAS FOUND AT THE FOLLOWING WEBSITE - http://forum.unity3d.com/threads/moving-main-camera-with-mouse.119525/
    /* ### ORIGINAL CODE ###
        // Move Camera Script
         
        var horizontalSpeed : float = 40.0;
        var verticalSpeed : float = 40.0;
         
        function Update () {
           
            // If Right Button is clicked Camera will move.
            while (Input.GetMouseButtonDown(1)) {
            var h : float = horizontalSpeed * Input.GetAxis ("Mouse Y");
            var v : float = verticalSpeed * Input.GetAxis ("Mouse X");
            transform.Translate(v,h,0);
            }
         
        }
    */


    // CAMS MODIFIED VERSION, WHICH ALLOWS THE USER TO MOVE ONLY A SET NUMBER OF PIXELS AWAY FROM THE CENTRE

    // HOLD DOWN THE RIGHT MOUSE BUTTON AND MOVE THE MOUSE TO SHIFT ACROSS THE PAGE
    if (Input.GetButton("Fire2") )
    {
        // HORIZONTAL MOVEMENT 
        var h : float = horizontalSpeed * Input.GetAxis ("Mouse X");

        // VERTICAL MOVEMENT
        var v : float = verticalSpeed * Input.GetAxis ("Mouse Y");


        // RESTRICTS HORIZONTAL MOVEMENT TO WITHIN allowedRadiusFromCentre PIXELS OF EITHER SIDE OF ZERO
        if ( ( ( horizontalPosition + h ) <= allowedRadiusFromCentre ) && ( ( horizontalPosition + h ) > -allowedRadiusFromCentre ) )
        {
            horizontalPosition += h;
            transform.Translate(h,0,0);
        }

        // RESTRICTS HORIZONTAL MOVEMENT TO WITHIN allowedRadiusFromCentre PIXELS OF EITHER SIDE OF ZERO
        if ( ( ( verticalPosition + v) <= allowedRadiusFromCentre ) && ( ( verticalPosition + v ) > -allowedRadiusFromCentre ) )
        {
            verticalPosition += v;
            transform.Translate(0,v,0);
        }

        // DISPLAY THE CURRENT HORIZONTAL AND VERTICAL POSITIONS TO THE CONSOLE
        print( "Current Horizontal Position: " + horizontalPosition + " : " + "Current Vertical Position: " + verticalPosition );
    }
    

    // THE ZOOM FUNCTIONS WERE FOUND AT THE FOLLOWING WEBSITE - http://forum.unity3d.com/threads/simple-camera-zooming-script-for-absolute-newbie.72460/
    // -------------------Code for Zooming Out------------
    if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (Camera.main.fieldOfView<=125)
                Camera.main.fieldOfView +=2;
            if (Camera.main.orthographicSize<=20)
                                Camera.main.orthographicSize +=0.5;
 
        }
    // ---------------Code for Zooming In------------------------
     if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (Camera.main.fieldOfView>2)
                Camera.main.fieldOfView -=2;
            if (Camera.main.orthographicSize>=1)
                                Camera.main.orthographicSize -=0.5;
        }
    
    /*  ### THIS CODE WAS NOT REQUIRED ###
    // -------Code to switch camera between Perspective and Orthographic--------
     if (Input.GetKeyUp(KeyCode.B ))
    {
        if (Camera.main.orthographic==true)
            Camera.main.orthographic=false;
        else
            Camera.main.orthographic=true;
    }
    */
}

function Start () 
{

}