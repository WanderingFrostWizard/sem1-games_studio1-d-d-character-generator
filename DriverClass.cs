﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*#########################################################################################################################
#                                                                                                                         #
#  DUNGEONS AND DRAGONS CHARACTER GENERATOR                                                                               #
#                                                                                                                         #
#  WRITTEN BY                                                                                                             #
#  - CAMERON WATT        - s3589163                                                                                       #
#  - CAMILLA MARIA MOSES - s3590513                                                                                       #
#                                                                                                                         #
#  THIS FILE SIMPLY CALLS THE MAIN MENU CLASS, TO START THE PROGRAM                                                       #                                                                                                    #
#                                                                                                                         #
#########################################################################################################################*/

namespace CCS_D_D_Character_Generator
{
    public class DriverClass
    {
        static void Main(string [] args)
        {
            // CREATE NEW INSTANCE OF THE CHARACTER GENERATOR
            MainMenu CharacterGenerator = new MainMenu();
        }
    }

}
