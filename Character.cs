/*#########################################################################################################################
#                                                                                                                         #
#  DUNGEONS AND DRAGONS CHARACTER GENERATOR - C# CONSOLE VERSION   -   CURRENT VERSION - 10    26 MAY 2016                #
#                                                                                                                         #
#  WRITTEN BY                                                                                                             #
#  - CAMERON WATT        - s3589163                                                                                       #
#  - CAMILLA MARIA MOSES - s3590513                                                                                       #
#                                                                                                                         #
#  THIS FILE IS DESIGNED TO BE GENERATE THE VALUES FOR A STANDARD (VERSION 5) D&D CHARACTER AND OUTPUT IT TO THE          #
#     SCREEN. THIS WILL BE MODIFIED AT A LATER DATE TO BE INTERFACED WITH A 2D CHARACTER SHEET GENERATED WITH UNITY       #
#                                                                                                                         #
#  THIS PROGRAM ACCOMPLISHES THE CHARACTER GENERATION USING FIVE MAIN STEPS (WHICH IS THE SAME STEPS USED TO CREATE A     #
#     CHARACTER ON PAPER)                                                                                                 #
#       1 - RACE                                                                                                          #
#       2 - CLASS                                                                                                         #
#       3 - ABILITY SCORES + MODIFIERS                                                                                    #
#       4 - DESCRIBE YOUR CHARACTER AND BACKGROUND                                                                        #
#       5 - EQUIPMENT                                                                                                     #
#                                                                                                                         #
#########################################################################################################################*/

using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCS_D_D_Character_Generator
{
    public class Character
    {
        private int roll = 0;  // VARIABLE TO STORE THE RESULTS OF RANDOM DICE ROLLS

        // CHARACTER INFORMATION
        private string characterName = "";
       
        private string characterClass = "";
        private int characterLevel    = 1;         // ALL NEW CHARACTERS START AT LEVEL 1
        private string characterBackground = "";
        private string playerName     = "Camo and Camila";
        private string race           = "";
        private string alignment      = "";
        private string experience     = "0";

        // 3A - STATS                                                                 
        private int strength     = 0;
        private int dexterity    = 0;
        private int constitution = 0;
        private int intelligence = 0;
        private int wisdom       = 0;
        private int charisma     = 0;

        // 3B - STAT MODIFIERS
        private int strengthModifier     = 0;
        private int dexterityModifier    = 0;
        private int constitutionModifier = 0;
        private int intelligenceModifier = 0;
        private int wisdomModifier       = 0;
        private int charismaModifier     = 0;

        private int inspiration = 99;          // THIS VALUE CAN ONLY BE ASSIGNED WHEN THE GAME IS IN PROGRESS
        private int proficiency = 0;

        // 3C - SAVING THROWS
        private int strengthSavingThrow     = 0;
        private int dexteritySavingThrow    = 0;
        private int constitutionSavingThrow = 0;
        private int intelligenceSavingThrow = 0;
        private int wisdomSavingThrow       = 0;
        private int charismaSavingThrow     = 0;

        // 3D - SKILLS  Acrobatics etc 
        // THIS ENUM IS USED TO ACCESS THE skillsArray TO ALLOW EASY REFERENCE TO EACH ELEMENT
        private enum skillType{ acrobatics, animalHandling, arcana, athletics, deception, history, insight, intimidation, investigation, medicine, nature, perception, performance, persuasion, religion, sleightOfHand, stealth, survival };

        private int[] skillsArray = new int[ 18 ];   // CREATE THE ARRAY WITH THE TOTAL NUMBER OF SKILLS WITHIN THE GAME

        // 3E - EXCESS STATS
        private int passiveWisdom       = 0;
        private int armorClass          = 0;
        private int initiative          = 0;   
        private string characterSpeed   = "";
        private int hitPointsMax        = 0;
        private int temporaryHitPoints  = 0;    // THIS IS INTENTIONALY LEFT BLANK THROUGHOUT THE PROGRAM - AS IT WILL ONLY BE USED WHEN THE CHARACTER IS PLAYED
        private string hitDice          = "";


        // 4 - BACKGROUND INFORMATION - SHOWN AS ARRAY LISTS TO CONTAIN EACH OF THE GIVEN BONUSES DESCRIBED AS stringS
        // THE PROFICIENCES AND LANGUAGES ARE STORED WITHIN THE ONE TEXTBOX, BUT STORED IN TWO DIFFERENT ARRAY LISTS
        private ArrayList proficienciesList     = new ArrayList();
        private ArrayList languageList          = new ArrayList();

        // EACH OF THE POSSIBLE LANGUAGES IN THE GAME. USED TO ADD EXTRA LANGUAGES
        string [] allLanguages = new string [] { "Common", "Dwarvish", "Elvish", "Giant", "Gnomish", "Goblin", "Halfling", "Orc", "Abyssal", "Celestial", "Draconic", "Deep Speech", "Infernal", "Primordial", "Sylvan", "Undercommon" };
 

        private ArrayList personalityTraitList  = new ArrayList();
        private ArrayList idealsList            = new ArrayList();
        private ArrayList bondsList             = new ArrayList();
        private ArrayList flawsList             = new ArrayList();
        private ArrayList featureTraitList      = new ArrayList();


        // 5 - EQUIPMENT
        private int copperPieces    = 0;
        private int silverPieces    = 0;
        private int electrumPieces  = 0;
        private int goldPieces      = 0;
        private int platinumPieces  = 0;
        private ArrayList equipmentList = new ArrayList();

        // WHEN OUTPUTTING THE languageList ARRAY, EACH LANGUAGE IS CONCATONATED INTO A SINGLE STRING AND STORED WITHIN THIS VARIABL
        private string languagesAsString = "";  

        Random dice = new Random();  // CODE FOR CALCULATING ALL RANDOM NUMBERS  


        // ######### CHARACTER NAMES ######### 
        // ### DWARF NAMES ###
        string [] dwarfFirstNames = new string [] { "Adrik", "Alberich", "Baern", "Barendd", "Brottor", "Bruenor", "Dain", "Darrak", "Delg", "Eberk", "Einkil", "Fargrim", "Flint", "Gardain", "Harbek", "Kildrak", "Morgran", "Orsik",
                                        "Oskar", "Rangrim", "Rurik", "Taklinn", "Thoradin", "Thorin", "Tordek", "Traubon", "Travok", "Ulfgar", "Veit", "Vondal", "Amber", "Artin", "Audhild", "Bardryn", "Dagnal",
                                        "Diesa", "Eldeth", "Falkrunn", "Finellen", "Gunnloda", "Gurdis", "Helja", "Hlin", "Kathra", "Kristryd", "Ilde", "Liftrasa", "Mardred", "Riswynn", "Sannl", "Torbera", "Torgga", "Vistra" };

        string [] dwarfClanNames = new string [] { "Balderk", "Battlehammer", "Brawnanvil", "Dankil", "Fireforge", "Frostbeard", "Gorunn", "Holderhek", "Ironfist", "Loderr", "Lutgehr", "Rumnaheim", "Strakeln", "Torunn", "Ungart" };


        // ### ELF NAMES ###
        string [] elfFirstNames = new string [] { "Adran", "Aelar", "Aramil", "Arannis", "Aust", "Beiro", "Berrian", "Carric", "Enialis", "Erdan", "Erevan", "Galinndan", "Hadarai", "Heian", "Himo", "Immeral", "Ivellios",
                                                    "Laucian", "Mindartis", "Paelias", "Peren", "Quarion", "Riardon", "Rolen", "Soveliss", "Thamior", "Tharivol", "Theren", "Varis", "Adrie", "Althaea", "Anastrianna",
                                                    "Andraste", "Antinua", "Bethrynna", "Birel", "Caelynn", "Drusilia", "Enna", "Felosial", "Ielenia", "Jelenneth", "Keyleth", "Leshanna", "Lia", "Meriele", "Mialee", "Naivara", "Quelenna", "Quillathe",
                                                    "Sariel", "Shanairra", "Shava", "Silaqui", "Theirastra", "Thia", "Vadania", "Valanthe", "Xanaphia" };

        string [] elfSurnames = new string [] { "Amakiir (Gemflower)", "Amastacia (Starflower)", "Galanodel (Moonwhisper)", "Holimion (Diamonddew)", "Ilphelkiir (Gemblossom)", "Liadon (Silverfrond)", 
                                                    "Meliamne (Oakenheel)", "Naïlo (Nightbreeze)", "Siannodel (Moonbrook)", "Xiloscient (Goldpetal)" };

        // ### HALFING NAMES ###
        string [] halfingFirstNames = new string [] { "Alton", "Ander", "Cade", "Corrin", "Eldon", "Errich", "Finnan", "Garret", "Lindal", "Lyle", "Merric", "Milo", "Osborn", "Perrin", "Reed", "Roscoe", "Wellby",
                                                "Andry", "Bree", "Callie", "Cora", "Euphemia", "Jillian", "Kithri", "Lavinia", "Lidda", "Merla", "Nedda", "Paela", "Portia", "Seraphina", "Shaena", "Trym", "Vani", "Verna" };

        string [] halfingSurnames = new string [] { "Brushgather", "Goodbarrel", "Greenbottle", "High-hill", "Hilltopple", "Leagallow", "Tealeaf", "Thorngage", "Tosscobble", "Underbough" };

        // ### HUMAN NAMES ###

        // TODO ####################################################################################################################################################################### 
        // AT THIS STAGE, EACH OF THE HUMAN NAMES HAVE BEEN ADDED TOGETHER
        string [] humanFirstNames = new string [] { "Aseir", "Bardeid", "Haseid", "Khemed", "Mehmen", "Sudeiman", "Zasheir", "Atala", "Ceidil", "Hama", "Jasmal", "Meilil", "Seipora", "Yasheira", "Zasheida",
                                "Darvin", "Dorn", "Evendur", "Gorstag", "Grim", "Helm", "Malark", "Morn", "Randal", "Stedd", "Arveene", "Esvele", "Jhessail", "Kerri", "Lureene", "Miri", "Rowan", "Shandri", "Tessele",
                                "Bor", "Fodel", "Glar", "Grigor", "Igan", "Ivor", "Kosef", "Mival", "Orel", "Pavel", "Sergor", "Alethra", "Kara", "Katernin", "Mara", "Natali", "Olma", "Tana", "Zora",
                                "Ander", "Blath", "Bran", "Frath", "Geth", "Lander", "Luth", "Malcer", "Stor", "Taman", "Urth", "Amafrey", "Betha", "Cefrey", "Kethra", "Mara", "Olga", "Silifrey", "Westra",
                                "Aoth", "Bareris", "Ehput-Ki", "Kethoth", "Mumed", "Ramas", "So-Kehur", "Thazar-De", "Urhur", "Arizima", "Chathi", "Nephis", "Nulara", "Murithi", "Sefris", "Thola", "Umara", "Zolis",
                                "Borivik", "Faurgar", "Jandar", "Kanithar", "Madislak", "Ralmevik", "Shaumar", "Vladislak", "Fyevarra", "Hulmarra", "Immith", "Imzel", "Navarra", "Shevarra", "Tammith", "Yuldra",
                                "An", "Chen", "Chi", "Fai", "Jiang", "Jun", "Lian", "Long", "Meng", "On", "Shan", "Shui", "Wen", "Bai", "Chao", "Jia", "Lei", "Mei", "Qiao", "Shui", "Tai",
                                "Anton", "Diero", "Marcon", "Pieron", "Rimardo", "Romero", "Salazar", "Umbero", "Balama", "Dona", "Faila", "Jalana", "Luisa", "Marta", "Quara", "Selise", "Vonda" };

        string [] humanSurnames = new string [] {"Basha", "Dumein", "Jassan", "Khalid", "Mostana", "Pashar", "Rein", 
                                "Amblecrown", "Buckman", "Dundragon", "Evenwood", "Greycastle", "Tallstag", 
                                "Bersk", "Chernin", "Dotsk", "Kulenov", "Marsk", "Nemetsk", "Shemov", "Starag",
                                "Brightwood", "Helder", "Hornraven", "Lackman", "Stormwind", "Windrivver",
                                "Ankhalab", "Anskuld", "Fezim", "Hahpet", "Nathandem", "Sepret", "Uuthrakt",
                                "Chergoba", "Dyernina", "Iltazyara", "Murnyethara", "Stayanoga", "Ulmokina", 
                                "Chien", "Huang", "Kao", "Kung", "Lao", "Ling", "Mei", "Pin", "Shin", "Sum", "Tan", "Wan",
                                "Agosto", "Astorio", "Calabra", "Domine", "Falone", "Marivaldi", "Pisacar", "Ramondo" };
        
        // ########################################################################################################################
        // THE MAIN FUNCTION CALLS THIS CONSTRUCTOR, WHICH GENERATES A CHARACTER AND PRINTS THE OUTPUT
        // ########################################################################################################################
        public Character()
        {
            // CLEAR VARIABLES TO ALLOW A NEW CHARACTER TO BE GENERATED
            clearVariables(); 

            // ### 1 ### CALCULATE RACE
            selectRace();

            // ### 2 ### CALCULATE CLASS
            selectClass();

            // ### 3 ### CALCULATE ABILITY SCORES
            calculateAbilityScores();

            // ### 4 ### DESCRIBE YOUR CHARACTER
            selectBackground();

            // ### 5 ### EQUIPMENT
                // TODO


            // TODO ###################################################################################################################################################################
            // SET THIS TO BE BASED UPON OTHER CHOICES THROUGHOUT THE PROGRAM
            chooseAlignment();     // SELECT A RANDOM ALIGNMENT

            printToConsole();

        } //   CLOSE void GenerateCharacter()


        // ### 1 - RACE METHODS ###
        public void selectRace()
        {
            roll = diceRoll( 1, 4 );     // SELECT A RANDOM CHARACTER RACE
            switch ( roll )
            {
                case 1:
                    {
                        createDwarf();
                        break;
                    }
                case 2:
                    {
                        createElf();
                        break;
                    }
                case 3:
                    {
                        createHalfling();
                        break;
                    }
                case 4:
                    {
                        createHuman();
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID RACE SELECTED" );
                        break;
                    }
            }
        }


        public void createDwarf()
        {
            roll = diceRoll( 1, 2 ) ;     // SELECT A RANDOM SUB-RACE AND SET VALUES
            switch ( roll )
            {
                case 1:
                    {
                        race = "Hill Dwarf";
                        wisdom = wisdom + 1;
                        featureTraitList.Add( "Dwarven Toughness: Your hit point maximum increases by 1, and it increases by 1 every time you gain a level" );
                        break;
                    }
                case 2:
                    {
                        race = "Mountain Dwarf";
                        strength = strength + 2;
                        proficienciesList.Add( "Dwarven Armor Training: You have proficiency with light and medium armor" );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID SUBRACE SELECTED" );
                        break;
                    }
            }

            // SET GENERIC RACIAL BONUSES
            constitution = constitution + 2;
            /* TODO #######################################################################################################################################################################               
            Alignment.Most dwarves are lawful, believing firmly in the benefits of a well - ordered society.They tend toward
            good as well, with a strong sense of fair play and a belief that everyone deserves to share in the benefits of a just order.
            */

            characterSpeed = "25 feet";     // Your speed is not reduced by wearing heavy armor.
            
            featureTraitList.Add( "Darkvision: Accustomed to life underground, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray. " );
            featureTraitList.Add( "Dwarven Resilience: You have advantage on saving throws against poison, and you have resistance against poison damage " );
            proficienciesList.Add( "Battleaxe, Handaxe, Light Hammer, Warhammer, " );

            // DWARVES GAIN PROFICIENCY WITH THE ARTISAN'S TOOLS OF THEIR CHOICE: smith’s tools, brewer’s supplies, or mason’s tools
            roll = diceRoll( 1, 2 );     // SELECT A RANDOM SUB-RACE AND SET VALUES
            switch ( roll )
            {
                case 1:
                    {
                        proficienciesList.Add( "smith’s tools, " );
                        break;
                    }
                case 2:
                    {
                        proficienciesList.Add( "brewer’s supplies, " );
                        break;
                    }
                case 3:
                    {
                        proficienciesList.Add( "mason’s tools, " );
                        break;
                    } 
            }

            featureTraitList.Add( "Stonecunning: Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your normal proficiency bonus " );
            languageList.Add( "Common, " );
            languageList.Add( "Dwarvish, " );

            // CREATE A RANDOM DWARF CHARACTER NAME FROM THE DWARF NAMES ARRAYS
            characterName = dwarfFirstNames [ diceRoll ( 0, dwarfFirstNames.Length - 1 ) ]  + " " + dwarfClanNames [ diceRoll ( 0, dwarfClanNames.Length - 1 ) ];
        }


        public void createElf()
        {
            roll = diceRoll( 1, 2 );     // SELECT A RANDOM SUB-RACE AND SET VALUES
            switch ( roll )
            {
                case 1:
                    {
                        race = "High Elf";
                        intelligence = intelligence + 1;
                        characterSpeed = "30 feet";

                        // TODO ##################################################################################################################################################################
                        // Cantrip.You know one cantrip of your choice from the wizard spell list. Intelligence is your spellcasting ability for it.
                        
                        // HIGH ELVES LEARN TO SPEAK, READ AND WRITE ONE EXTRA LANGUAGE OF THEIR CHOICE
                        addExtraLanguage( 1 );
                        break;
                    }
                case 2:
                    {
                        race = "Wood Elf";
                        wisdom = wisdom + 1;
                        characterSpeed = "35 feet";
                        featureTraitList.Add("Mask of the Wild: You can attempt to hide even when you are only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena ");
                        break;
                    }
                default:
                    {
                        invalidInput("ERROR INVALID SUBRACE SELECTED");
                        break;
                    }
            }

            // THE "Elf Weapon Training" IS COMMON TO BOTH SUBRACES
            proficienciesList.Add("longsword, shortsword, shortbow, longbow, ");

            dexterity = dexterity + 2;

            /* TODO #######################################################################################################################################################################
            Alignment. Elves love freedom, variety, and selfexpression, so they lean strongly toward the gentler aspects of chaos. They value and protect others’ 
            freedom as well as their own, and they are more often good than not.
            */
            featureTraitList.Add( "Darkvision: Accustomed to twilit forests and the night sky, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray " );

            // KEEN SENSES: YOU HAVE PROFICIENCY IN THE PERCEPTION SKILL
            skillsArray[ (int) skillType.perception ] += proficiency;

            featureTraitList.Add( "Fey Ancestry: You have advantage on saving throws against being charmed, and magic can’t put you to sleep. " );
            featureTraitList.Add( "Trance: Elves don’t need to sleep. Instead, they meditate deeply, remaining semiconscious, for 4 hours a day. (The Common word for such meditation is “trance.”) While meditating, you can dream after a fashion; such dreams are actually mental exercises that have become reflexive through years of practice. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep " );
            languageList.Add( "Common, " );
            languageList.Add( "Elvish, " );

            // CREATE A RANDOM ELF CHARACTER NAME FROM THE ELF NAMES ARRAYS
            characterName = elfFirstNames [ diceRoll ( 0, elfFirstNames.Length - 1 ) ]  + " " + elfSurnames [ diceRoll ( 0, elfSurnames.Length - 1 ) ];
        }


        public void createHalfling()
        {
            roll = diceRoll( 1, 2 );     // SELECT A RANDOM SUB-RACE AND SET VALUES
            switch ( roll )
            {
                case 1:
                    {
                        race = "Lightfoot Halfling";
                        charisma = charisma + 1;
                        featureTraitList.Add( "Naturally Stealthy: You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you " );
                        break;
                    }
                case 2:
                    {
                        race = "Stout Halfling";
                        constitution = constitution + 1;
                        featureTraitList.Add( "Stout Resilience: You have advantage on saving throws against poison, and you have resistance against poison damage " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID SUBRACE SELECTED" );
                        break;
                    }
            }

            /* TODO #######################################################################################################################################################################
            Alignment. Most halflings are lawful good. As a rule, they are good-hearted and kind, hate to see others in pain, and have no tolerance for oppression. 
            They are also very orderly and traditional, leaning heavily on the support of their community and the comfort of their old ways.
            */
            characterSpeed = "25 feet";
            dexterity = dexterity + 2;
            featureTraitList.Add( "Lucky: When you roll a 1 on the d20 for an attack roll, ability check, or saving throw, you can re-roll the die and must use the new roll " );
            featureTraitList.Add( "Brave: You have advantage on saving throws against being frightened " );
            featureTraitList.Add( "Halfling Nimbleness. You can move through the space of any creature that is of a size larger than yours " );
            languageList.Add( "Common, " );
            languageList.Add( "Halfling, " );

            // CREATE A RANDOM HALFING CHARACTER NAME FROM THE HALFING NAMES ARRAYS
            characterName = halfingFirstNames [ diceRoll ( 0, halfingFirstNames.Length - 1 ) ]  + " " + halfingSurnames [ diceRoll ( 0, halfingSurnames.Length - 1 ) ];
        }


        public void createHuman()
        {
            roll = diceRoll( 1, 9 );     // SELECT A RANDOM SUB-RACE AND SET VALUES
            switch ( roll )
            {
                case 1:
                    {
                        race = "Human (Calshite)";
                        break;
                    }
                case 2:
                    {
                        race = "Human (Chondathan)";
                        break;
                    }
                case 3:
                    {
                        race = "Human (Damaran)";
                        break;
                    }
                case 4:
                    {
                        race = "Human (Illuskan)";
                        break;
                    }
                case 5:
                    {
                        race = "Human (Mulan)";
                        break;
                    }
                case 6:
                    {
                        race = "Human (Rashemi)";
                        break;
                    }
                case 7:
                    {
                        race = "Human (Shou)";
                        break;
                    }
                case 8:
                    {
                        race = "Human (Tethyrian)";
                        break;
                    }
                case 9:
                    {
                        race = "Human (Turami)";
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID SUBRACE SELECTED" );
                        break;
                    }
            }

            // ALL HUMANS GET EACH ABILITY INCREASED BY ONE
            strength = strength + 1;
            dexterity = dexterity + 1; 
            constitution = constitution + 1;
            intelligence = intelligence + 1;
            wisdom = wisdom + 1;
            charisma = charisma + 1;

            /* TODO #######################################################################################################################################################################
            Alignment.Humans tend toward no particular alignment.The best and the worst are found among them.
             */
            characterSpeed = "30 feet";

            languageList.Add( "Common, " );

            // HUMANS LEARN TO SPEAK, READ AND WRITE THE COMMON TONGUE, PLUS ONE EXTRA LANGUAGE OF THEIR CHOICE
            addExtraLanguage( 1 );


            // CREATE A RANDOM HUMAN CHARACTER NAME FROM THE HUMAN NAMES ARRAYS ( INCLUDES ALL SUB-RACES AT THIS STAGE )
            characterName = humanFirstNames [ diceRoll ( 0, humanFirstNames.Length - 1 ) ]  + " " + humanSurnames [ diceRoll ( 0, humanSurnames.Length - 1 ) ];
        }


        // ### 2 - CLASS METHODS ###
        public void selectClass()
        {
            roll = diceRoll( 1, 4 );     // SELECT A RANDOM CHARACTER CLASS
            switch ( roll )
            {
                case 1:
                    {
                        createCleric();
                        break;
                    }
                case 2:
                    {
                        createFighter();
                        break;
                    }
                case 3:
                    {
                        createRogue();
                        break;
                    }
                case 4:
                    {
                        createWizard();
                        break;
                    }
                default:
                    {
                        invalidInput("ERROR INVALID CLASS SELECTED");
                        break;
                    }
            }
        }


        public void createCleric()
        {
            // TODO
            // Skills: Choose two from History, Insight, Medicine, Persuasion, and Religion
            
            characterClass = "Cleric";
            hitDice = "1d8";   // PER CLERIC LEVEL
            hitPointsMax = 8;   //  + constitution  WHICH IS ADDED WHEN THE STATS ARE ROLLED IN STEP 3

            proficiency = 2;     // LEVEL ONE CLERICS HAVE +2 proficiency
            proficienciesList.Add( "Light Armor, Medium Armor, Shields, Simple weapons" );

            // CLERICS RECEIVE BONUSES TO STRENGTH AND CONSTITUTION THROWS. ADD THE BONUSES, AND SHOW THE RELEVANT MARKERS ON THE CHARACTER SHEET
            wisdomSavingThrow = wisdomSavingThrow + proficiency;
            charismaSavingThrow = charismaSavingThrow + proficiency;

            // CHOICE OF EQUIPMENT
            // (a) a mace or (b) a warhammer (if proficient)
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "Mace, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "Warhammer (if proficient)" );  // TODO  ####################################################################################################################
            }

            // (a) scale mail, (b) leather armor, or (c) chain mail (if proficient)
            roll = diceRoll( 1, 3 );
            if ( roll == 1 )  
            { 
                equipmentList.Add( "Scale mail, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "Leather Armor, " );
            }
            else if ( roll == 3 )
            {
                equipmentList.Add( "Chain mail (if proficient), " ); // TODO  ####################################################################################################################
            }

            // (a) a light crossbow and 20 bolts or (b) any simple weapon
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "light crossbow, 20 bolts, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "any simple weapon, " );
            }

            // (a) a priest’s pack or (b) an explorer’s pack
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                addPriestsPack();
            }
            else if ( roll == 2 )
            {
                addExplorersPack();
            }

            // ALL CLERICS RECEIVE "A shield and a holy symbol"
            equipmentList.Add( "A shield, holy symbol, " );
        } // END OF createCleric


        public void createFighter()
        {
            characterClass = "Fighter";
            hitDice = "1d10"; // PER FIGHTER LEVEL
            hitPointsMax = 10;   //  + constitution  WHICH IS ADDED WHEN THE STATS ARE ROLLED IN STEP 3

            proficiency = 2;     // LEVEL ONE FIGHTERS HAVE +2 proficiency
            proficienciesList.Add("All Armor, Shields, Simple Weapons, Martial Weapons");

            // FIGHTERS RECEIVE BONUSES TO STRENGTH AND CONSTITUTION THROWS
            strengthSavingThrow = strengthSavingThrow + proficiency;
            constitutionSavingThrow = constitutionSavingThrow + proficiency;

            // CHOICE OF EQUIPMENT
            // (a)chain mail or (b)leather armor, longbow, and 20 arrows
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "Chain Mail, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "Leather Armor, Longbow, 20 Arrows, " );
            }

            // (a)a martial weapon and a shield or (b)two martial weapons
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "a martial weapon and a shield, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "two martial weapons, " );
            }

            // (a)a light crossbow and 20 bolts or(b) two handaxes
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "light crossbow, 20 bolts, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "two handaxes, " );
            }

            // (a)a dungeoneer’s pack or(b) an explorer’s pack
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                addDungeoneersPack();
            }
            else if ( roll == 2 )
            {
                addExplorersPack();
            }

            // TODO #######################################################################################################################################################################
            //  Skills: Choose two skills from Acrobatics, Animal Handling, Athletics, History, Insight, Intimidation, Perception, and Survival

        }   // END OF createFighter


        public void createRogue()
        {
            // TODO #######################################################################################################################################################################
            //    Skills: Choose four from Acrobatics, Athletics, Deception, Insight, Intimidation, Investigation, Perception, Performance, Persuasion, Sleight of Hand, and Stealth

            characterClass = "Rogue";
            hitDice = "1d8"; // PER ROGUE LEVEL
            hitPointsMax = 8;   //  + constitution  WHICH IS ADDED WHEN THE STATS ARE ROLLED IN STEP 3

            proficiency = 2;     // LEVEL ONE ROGUES HAVE +2 proficiency
            proficienciesList.Add( "Light Armor, Simple weapons, hand crossbows, longswords, rapiers, shortswords, " );
            proficienciesList.Add( "Thieves’ Tools, " );

            // ROGUES RECEIVE BONUSES TO DEXTERITY AND INTELLIGENCE THROWS
            dexteritySavingThrow = dexteritySavingThrow + proficiency;
            intelligenceSavingThrow = intelligenceSavingThrow + proficiency;

            // CHOICE OF EQUIPMENT
            // (a)a rapier or (b)a shortsword
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "Rapier, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "Shortsword, " );
            }

            // (a)a shortbow and quiver of 20 arrows or (b) a shortsword
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "shortbow, quiver of 20 arrows, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "Shortsword, " );
            }

            // (a)a burglar’s pack, (b)a dungeoneer’s pack, or(c) an explorer’s pack
            roll = diceRoll( 1, 3 );
            if ( roll == 1 )         
            {
                addBurglarsPack();
            }
            else if ( roll == 2 )    
            {
                addDungeoneersPack();                
            }
            else if ( roll == 3 )  
            {
                addExplorersPack();                
            }

            // ALL ROGUES RECEIVE Leather armor, two daggers, and thieves’ tools
            equipmentList.Add( "Leather armor, two daggers, thieves’ tools, " );

        }   // END OF createRogue

        public void createWizard()
        {
            characterClass = "Wizard";
            hitDice = "1d6"; // PER WIZARD LEVEL
            hitPointsMax = 6;   //  + constitution  WHICH IS ADDED WHEN THE STATS ARE ROLLED IN STEP 3

            proficiency = 2;     // LEVEL ONE WIZARDS HAVE +2 proficiency
            proficienciesList.Add( "Daggers, darts, slings, quarterstaffs, light crossbows, " );

            // WIZARDS RECEIVE BONUSES TO INTELLIGENCE AND WISDOM THROWS
            intelligenceSavingThrow = intelligenceSavingThrow + proficiency;
            wisdomSavingThrow = wisdomSavingThrow + proficiency;

            // CHOICE OF EQUIPMENT
            // (a)a quarterstaff or (b)a dagger
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "quarterstaff, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "Dagger, " );
            }

            // (a)a component pouch or(b) an arcane focus
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )
            {
                equipmentList.Add( "component pouch, " );
            }
            else if ( roll == 2 )
            {
                equipmentList.Add( "arcane focus, " );
            }

            // (a)a scholar’s pack or(b) an explorer’s pack
            roll = diceRoll( 1, 2 );
            if ( roll == 1 )         
            {
                addScholarsPack();
            }
            else if ( roll == 2 )    
            {
                addExplorersPack();
            }

            // ALL WIZARDS RECEIVE A spellbook
            equipmentList.Add("Spellbook, ");

            // TODO #######################################################################################################################################################################
            // Skills: Choose two from Arcana, History, Insight, Investigation, Medicine, and Religion

        }   // END OF createWizard


        // ### 3 - STATS METHODS ###
        private void calculateAbilityScores()
        {
            // CALCULATE STATS
            strength     = RollStats();
            dexterity    = RollStats();
            constitution = RollStats();
            intelligence = RollStats();
            wisdom       = RollStats();
            charisma     = RollStats();

            hitPointsMax = hitPointsMax + constitution;    // EACH CHARACTER TYPE ADDS THEIR CONSTITUTION TO THEIR RACIAL/CLASS BONUS

            // CALCULATE STAT MODIFIERS
            strengthModifier     = calculateStatModifier(strength);
            dexterityModifier    = calculateStatModifier(dexterity);
            constitutionModifier = calculateStatModifier(constitution);
            intelligenceModifier = calculateStatModifier(intelligence);
            wisdomModifier       = calculateStatModifier(wisdom);
            charismaModifier     = calculateStatModifier(charisma);

            // CALCULATE SAVING THROWS IS EQUAL TO THE MODIFIER + CLASS BONUSES TO THROWS ( WHICH WERE ADDED WHEN THE CLASSES WERE SELECTED )
            strengthSavingThrow     = strengthSavingThrow   + strengthModifier;
            dexteritySavingThrow    = dexteritySavingThrow  + dexterityModifier;
            constitutionSavingThrow = constitutionSavingThrow + constitutionModifier;
            intelligenceSavingThrow = intelligenceSavingThrow + intelligenceModifier;
            wisdomSavingThrow       = wisdomSavingThrow     + wisdomModifier;
            charismaSavingThrow     = charismaSavingThrow   + charismaModifier;

            // CALCULATE SKILLS - EACH IS EQUAL TO IT EQUIVALENT MODIFIER
            skillsArray[ (int) skillType.acrobatics     ] += dexterityModifier; 
            skillsArray[ (int) skillType.animalHandling ] += wisdomModifier;
            skillsArray[ (int) skillType.arcana         ] += intelligenceModifier;
            skillsArray[ (int) skillType.athletics      ] += strengthModifier;
            skillsArray[ (int) skillType.deception      ] += charismaModifier;
            skillsArray[ (int) skillType.history        ] += intelligenceModifier;
            skillsArray[ (int) skillType.insight        ] += wisdomModifier;
            skillsArray[ (int) skillType.intimidation   ] += charismaModifier;
            skillsArray[ (int) skillType.investigation  ] += intelligenceModifier;
            skillsArray[ (int) skillType.medicine       ] += wisdomModifier;
            skillsArray[ (int) skillType.nature         ] += intelligenceModifier;
            skillsArray[ (int) skillType.perception     ] += wisdomModifier;
            skillsArray[ (int) skillType.performance    ] += charismaModifier;
            skillsArray[ (int) skillType.persuasion     ] += charismaModifier;
            skillsArray[ (int) skillType.religion       ] += intelligenceModifier;
            skillsArray[ (int) skillType.sleightOfHand  ] += dexterityModifier;
            skillsArray[ (int) skillType.stealth        ] += dexterityModifier;
            skillsArray[ (int) skillType.survival       ] += wisdomModifier;

            // PASSIVE WISDOM = WISDOM MODIFIER + PROFICIENCY BONUS (IF PROFICIENT IN PERCEPTION)...   
            // AS THE VALUE FOR PERCEPTION IS CALCULATED THE SAME WAY (AND IS ALWAYS EQUAL TO), I AM SIMPLY REFERENCING IT
            passiveWisdom = 10 + skillsArray[ (int) skillType.perception ];

            // TODO  ########################## EQUIPMENT BONUSES #############################################################################################################################
            armorClass = 10 + dexterityModifier;    // SET THE CHARACTERS NATURAL ARMOR, BONUSES GIVEN BY ARMOR WILL BE ADDED LATER

            // INITIATIVE IS EQUAL TO THE DEXTERITY MODIFIER
            initiative = dexterityModifier;

        }   // END OF  private void calculateAbilityScores()


        // USED TO CALCULATE THE STAT ROLLS.  FOR EACH STAT, A PLAYER ROLLS FOUR D6 DICE, AND CHOOSES THE HIGHEST 3.
        // THIS METHOD STORES THE ROLLS INTO AN ARRAY, THEN SORTS THEM, BEFORE RETURNING THE SUM OF THE HIGHEST 3 ROLLS
        private int RollStats()
        {
            int [] rollsSorted = new int [4] { 0, 0, 0, 0 };

            for (int roll = 0; roll < 4; roll++)
            {
                rollsSorted [roll] = diceRoll(1, 6);
            }

            // SORT THE ARRAY FROM HIGHEST TO LOWEST
            Array.Sort(rollsSorted);

            // RETURN THE HIGHEST 3 ROLLS (IE THE LAST 3 WITHIN THE ARRAY)
            return (rollsSorted [1] + rollsSorted [2] + rollsSorted [3]);
        }


        // USE THIS FUNCTION TO CALCULATE THE STAT MODIFIER USING THE STAT REQUIRED AS THE ARGUMENT
        // eg     intelligenceModifier = calculateStatModifier(intelligence); etc
        private int calculateStatModifier(int statistic)      
        {
            int modifier = 0;

            switch (statistic)
            {
                case 1:
                    {
                        modifier = -5;
                        break;
                    }
                case 2:
                case 3:
                    {
                        modifier = -4;
                        break;
                    }
                case 4:
                case 5:
                    {
                        modifier = -3;
                        break;
                    }
                case 6:
                case 7:
                    {
                        modifier = -2;
                        break;
                    }
                case 8:
                case 9:
                    {
                        modifier = -1;
                        break;
                    }
                case 10:
                case 11:
                    {
                        modifier = 0;
                        break;
                    }
                case 12:
                case 13:
                    {
                        modifier = 1;
                        break;
                    }
                case 14:
                case 15:
                    {
                        modifier = 2;
                        break;
                    }
                case 16:
                case 17:
                    {
                        modifier = 3;
                        break;
                    }
                case 18:
                case 19:
                    {
                        modifier = 4;
                        break;
                    }
                case 20:
                case 21:
                    {
                        modifier = 5;
                        break;
                    }
                case 22:
                case 23:
                    {
                        modifier = 6;
                        break;
                    }
                case 24:
                case 25:
                    {
                        modifier = 7;
                        break;
                    }
                case 26:
                case 27:
                    {
                        modifier = 8;
                        break;
                    }
                case 28:
                case 29:
                    {
                        modifier = 9;
                        break;
                    }
                case 30:
                    {
                        modifier = 10;
                        break;
                    }
                default:
                    {
                        string x = "INCORRECT VALUE FOR CURRENT STATISTIC - CURRENTLY " + statistic;
                        invalidInput(x);
                        break;
                    }

            } // switch (statistic)
            return modifier;
        } // CLOSE private string calculateStatModifier(int statistic)


        // ### 4 - DESCRIBE YOUR CHARACTER METHODS ###
        private void selectBackground()
        {
            roll = diceRoll( 1, 6 );     // SELECT A RANDOM CHARACTER BACKGROUND
            switch ( roll )
            {
                case 1:
                    {
                        createAcolyte();
                        break;
                    }
                case 2:
                    {
                        createCriminal();
                        break;
                    }
                case 3:
                    {
                        createFolkHero();
                        break;
                    }
                case 4:
                    {
                        createNoble();
                        break;
                    }
                case 5:
                    {
                        createSage();
                        break;
                    }
                case 6:
                    {
                        createSoldier();
                        break;
                    }
                default:
                    {
                        invalidInput("ERROR INVALID BACKGROUND SELECTED");
                        break;
                    }
            }
        }


        public void createAcolyte()
        {
            characterBackground = "Acolyte";

            // ADD THE ACOLYTE SKILL PROFICIENCIES: Insight, Religion
            skillsArray[ (int) skillType.insight ]  += proficiency;
            skillsArray[ (int) skillType.religion ] += proficiency;

            // ACOLYTES LEARN TO SPEAK, READ AND WRITE TWO EXTRA LANGUAGEs OF THEIR CHOICE
            addExtraLanguage( 2 );

            equipmentList.Add( "Holy Symbol, 5 sticks of incense, vestments, set of common clothes, pouch, " );

            // TODO ## CHECK TO SEE IF CHARACTER ALREADY HAS A PRAYER BOOK, OR HOLY SYMBOL   ###############################################################################################
            // holy symbol(a gift to you when you entered the priesthood), prayer book or prayer wheel

            // ADD A Prayer Book OR Prayer Wheel
            roll = diceRoll( 1, 2);
            switch ( roll )
            {
                case 1:
                    {
                        equipmentList.Add( "Prayer Book, " ); 
                        break;
                    }
                case 2:
                    {
                        equipmentList.Add( "Prayer Wheel, " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID ITEM SELECTED" );
                        break;
                    }
            }

            goldPieces = goldPieces + 15;   // Acolytes START WITH 15gp

            string templesGod = "Oghma";            // NAME OF THE GOD WHICH YOUR TEMPLE IS DEVOTED TO
            string templesLocation = "Neverwinter"; // LOCATION OF THE TEMPLE YOU SERVED IN
            featureTraitList.Add("Shelter of the Faithful: As a servant of " + templesGod + " , you command the respect of those who share your faith, and you can perform the rites of " + templesGod + " . You and your companions can expect to receive free healing and care at a temple, shrine, or other established presence of " + templesGod + " faith. Those who share your religion will support you (and only you) at a modest lifestyle. You also have ties to the temple of " + templesGod + " in " + templesLocation + " , where you have a residence. When you are in " + templesLocation + " , you can call upon the priests there for assistance that won’t endanger them. ");
            

            // SET PERSONALITY TRAIT
            roll = diceRoll( 1, 8 );
            switch ( roll )
            {
                case 1:
                    {
                        personalityTraitList.Add( "I idolize a particular hero of my faith, and constantly refer to that person’s deeds and example. " );
                        break;
                    }
                case 2:
                    {
                        personalityTraitList.Add( "I can find common ground between the fiercest enemies, empathizing with them and always working toward peace. " );
                        break;
                    }
                case 3:
                    {
                        personalityTraitList.Add( "I see omens in every event and action. The gods try to speak to us, we just need to listen. " );
                        break;
                    }
                case 4:
                    {
                        personalityTraitList.Add( "Nothing can shake my optimistic attitude. " );
                        break;
                    }
                case 5:
                    {
                        personalityTraitList.Add( "I quote (or misquote) sacred texts and proverbs in almost every situation. " );
                        break;
                    }
                case 6:
                    {
                        personalityTraitList.Add( "I am tolerant(or intolerant) of other faiths and respect (or condemn) the worship of other gods. " );
                        break;
                    }
                case 7:
                    {
                        personalityTraitList.Add( "I’ve enjoyed fine food, drink, and high society among my temple’s elite. Rough living grates on me. " );
                        break;
                    }
                case 8:
                    {
                        personalityTraitList.Add( "I’ve spent so long in the temple that I have little practical experience dealing with people in the outside world. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID TRAIT SELECTED" );
                        break;
                    }
            }  // END OF SET PERSONALITY TRAIT


            // SET CHARACTER IDEALS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        idealsList.Add( "Tradition: The ancient traditions of worship and sacrifice must be preserved and upheld. (Lawful) " );
                        break;
                    }
                case 2:
                    {
                        idealsList.Add( "Charity: I always try to help those in need, no matter what the personal cost. (Good) " );
                        break;
                    }
                case 3:
                    {
                        idealsList.Add( "Change: We must help bring about the changes the gods are constantly working in the world. (Chaotic) " );
                        break;
                    }
                case 4:
                    {
                        idealsList.Add( "Power: I hope to one day rise to the top of my faith’s religious hierarchy. (Lawful) " );
                        break;
                    }
                case 5:
                    {
                        idealsList.Add( "Faith: I trust that my deity will guide my actions. I have faith that if I work hard, things will go well. (Lawful) " );
                        break;
                    }
                case 6:
                    {
                        idealsList.Add( "Aspiration: I seek to prove myself worthy of my god’s favor by matching my actions against his or her teachings. (Any) " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID IDEAL SELECTED" );
                        break;
                    }
            }  // END OF SET CHARACTER IDEALS


            // SET CHARACTER BONDS
            roll = diceRoll( 1, 6 ); 
            switch ( roll )
            {
                case 1:
                    {
                        bondsList.Add( "I would die to recover an ancient relic of my faith that was lost long ago. " );
                        break;
                    }
                case 2:
                    {
                        bondsList.Add( "I will someday get revenge on the corrupt temple hierarchy who branded me a heretic. " );
                        break;
                    }
                case 3:
                    {
                        bondsList.Add( "I owe my life to the priest who took me in when my parents died. " );
                        break;
                    }
                case 4:
                    {
                        bondsList.Add( "Everything I do is for the common people. " );
                        break;
                    }
                case 5:
                    {
                        bondsList.Add( "I will do anything to protect the temple where I served. " );
                        break;
                    }
                case 6:
                    {
                        bondsList.Add( "I seek to preserve a sacred text that my enemies consider heretical and seek to destroy. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID BOND SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER BONDS


            // SET CHARACTER FLAWS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        flawsList.Add( "I judge others harshly, and myself even more severely. " );
                        break;
                    }
                case 2:
                    {
                        flawsList.Add( "I put too much trust in those who wield power within my temple’s hierarchy. " );
                        break;
                    }
                case 3:
                    {
                        flawsList.Add( "My piety sometimes leads me to blindly trust those that profess faith in my god. " );
                        break;
                    }
                case 4:
                    {
                        flawsList.Add( "I am inflexible in my thinking. " );
                        break;
                    }
                case 5:
                    {
                        flawsList.Add( "I am suspicious of strangers and expect the worst of them. " );
                        break;
                    }
                case 6:
                    {
                        flawsList.Add( "Once I pick a goal, I become obsessed with it to the detriment of everything else in my life. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID FLAW SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER FLAWS

        }   // END OF createAcolyte();


        public void createCriminal()
        {
            characterBackground = "Criminal";

            // ADD THE CRIMINAL SKILL PROFICIENCIES: Deception, Stealth
            skillsArray[ (int) skillType.deception ] += proficiency;
            skillsArray[ (int) skillType.stealth ]   += proficiency;

            // ADD A GAMBLING PROFESSION
            selectGamblingProficiency();

            // TODO #######################################################################################################################################################################
            // TODO CHECK TO SEE IF PLAYER ALREADY HAS A SET OF THESE !!!!
            proficienciesList.Add( "Theives Tools, " );

            equipmentList.Add( "crowbar, set of dark common clothes, including a hood, pouch, " );
            goldPieces = goldPieces + 15;   // CRIMINALS START WITH 15 gp

            // SET PERSONALITY TRAITS
            roll = diceRoll( 1, 8 ) ;
            switch ( roll )
            {
                case 1:
                    {
                        personalityTraitList.Add( "I always have a plan for what to do when things go wrong. " );
                        break;
                    }
                case 2:
                    {
                        personalityTraitList.Add( "I am always calm, no matter what the situation. I never raise my voice or let my emotions control me. " );
                        break;
                    }
                case 3:
                    {
                        personalityTraitList.Add( "The first thing I do in a new place is note the locations of everything valuable, or where such things could be hidden. " );
                        break;
                    }
                case 4:
                    {
                        personalityTraitList.Add( "I would rather make a new friend than a new enemy. " );
                        break;
                    }
                case 5:
                    {
                        personalityTraitList.Add( "I am incredibly slow to trust. Those who seem the fairest often have the most to hide. " );
                        break;
                    }
                case 6: 
                    {
                        personalityTraitList.Add( "I don’t pay attention to the risks in a situation. Never tell me the odds. " );
                        break;
                    } 
                case 7: 
                    {
                        personalityTraitList.Add( "The best way to get me to do something is to tell me I can’t do it. " );
                        break;
                    }
                case 8:
                    {
                        personalityTraitList.Add( "I blow up at the slightest insult. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID TRAIT SELECTED" );
                        break;
                    }
            }  // END OF SET PERSONALITY TRAITS


            // SET CHARACTER IDEALS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        idealsList.Add( "Honor: I don’t steal from others in the trade. (Lawful) " );
                        break;
                    }
                case 2:
                    {
                        idealsList.Add( "Freedom: Chains are meant to be broken, as are those who would forge them. (Chaotic) " );
                        break;
                    }
                case 3:
                    {
                        idealsList.Add( "Charity: I steal from the wealthy so that I can help people in need. (Good) " );
                        break;
                    }
                case 4:
                    {
                        idealsList.Add( "Greed: I will do whatever it takes to become wealthy. (Evil) " );
                        break;
                    }
                case 5:
                    {
                        idealsList.Add( "People: I’m loyal to my friends, not to any ideals, and everyone else can take a trip down the Styx for all I care. (Neutral) " );
                        break;
                    }
                case 6:
                    {
                        idealsList.Add( "Redemption: There’s a spark of good in everyone. (Good) " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID IDEAL SELECTED" );
                        break;
                    }
            }  // END OF SET CHARACTER IDEALS


            // SET CHARACTER BONDS
            roll = diceRoll( 1, 6 ); 
            switch ( roll )
            {
                case 1:
                    {
                        bondsList.Add( "I’m trying to pay off an old debt I owe to a generous benefactor. " );
                        break;
                    }
                case 2:
                    {
                        bondsList.Add( "My ill-gotten gains go to support my family. " );
                        break;
                    }
                case 3:
                    {
                        bondsList.Add( "Something important was taken from me, and I aim to steal it back. " );
                        break;
                    }
                case 4:
                    {
                        bondsList.Add( "I will become the greatest thief that ever lived. " );
                        break;
                    }
                case 5:
                    {
                        bondsList.Add( "I’m guilty of a terrible crime. I hope I can redeem myself for it. " );
                        break;
                    }
                case 6:
                    {
                        bondsList.Add( "Someone I loved died because of I mistake I made. That will never happen again. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID BOND SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER BONDS


            // SET CHARACTER FLAWS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        flawsList.Add( "When I see something valuable, I can’t think about anything but how to steal it. " );
                        break;
                    }
                case 2:
                    {
                        flawsList.Add( "When faced with a choice between money and my friends, I usually choose the money. " );
                        break;
                    }
                case 3:
                    {
                        flawsList.Add( "If there’s a plan, I’ll forget it. If I don’t forget it, I’ll ignore it. " );
                        break;
                    }
                case 4:
                    {
                        flawsList.Add( "I have a “tell” that reveals when I’m lying. " );
                        break;
                    }
                case 5:
                    {
                        flawsList.Add( "I turn tail and run when things look bad. " );
                        break;
                    }
                case 6:
                    {
                        flawsList.Add( "An innocent person is in prison for a crime that I committed. I’m okay with that. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID FLAW SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER FLAWS
        }   // END OF createCriminal


        public void createFolkHero()
        {
            characterBackground = "Folk Hero";

            // ADD THE FOLK HERO SKILL PROFICIENCIES: Animal Handling, Survival
            skillsArray[ (int) skillType.animalHandling ] += proficiency;
            skillsArray[ (int) skillType.survival ] += proficiency; 

            // TODO #######################################################################################################################################################################
            // Tool Proficiencies: One type of artisan’s tools,    +   Equipment: A set of artisan’s tools (one of your choice),

            proficienciesList.Add( "vehicles (land), " );

            equipmentList.Add( "shovel, iron pot, set of common clothes, pouch, " );

            goldPieces = goldPieces + 10;   // FOLK HERO'S START WITH 10 gp

            // SET PERSONALITY TRAITS
            roll = diceRoll( 1, 8 );
            switch ( roll )
            {
                case 1:
                    {
                        personalityTraitList.Add( "I judge people by their actions, not their words. " );
                        break;
                    }
                case 2:
                    {
                        personalityTraitList.Add( "If someone is in trouble, I’m always ready to lend help. " );
                        break;
                    }
                case 3:
                    {
                        personalityTraitList.Add( "When I set my mind to something, I follow through no matter what gets in my way. " );
                        break;
                    }
                case 4:
                    {
                        personalityTraitList.Add( "I have a strong sense of fair play and always try to find the most equitable solution to arguments. " );
                        break;
                    }
                case 5:
                    {
                        personalityTraitList.Add( "I’m confident in my own abilities and do what I can to instill confidence in others. " );
                        break;
                    }
                case 6:
                    {
                        personalityTraitList.Add( "Thinking is for other people. I prefer action. " );
                        break;
                    }
                case 7:
                    {
                        personalityTraitList.Add( "I misuse long words in an attempt to sound smarter " );
                        break;
                    }
                case 8:
                    {
                        personalityTraitList.Add( "I get bored easily. When am I going to get on with my destiny ? " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID TRAIT SELECTED" );
                        break;
                    }
            }  // END OF SET PERSONALITY TRAITS


            // SET CHARACTERS IDEALS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        idealsList.Add( "Respect: People deserve to be treated with dignity and respect. (Good) " );
                        break;
                    }
                case 2:
                    {
                        idealsList.Add( "Fairness: No one should get preferential treatment before the law, and no one is above the law. (Lawful) " );
                        break;
                    }
                case 3:
                    {
                        idealsList.Add( "Freedom: Tyrants must not be allowed to oppress the people. (Chaotic) " );
                        break;
                    }
                case 4:
                    {
                        idealsList.Add( "Might: If I become strong, I can take what I want — what I deserve. (Evil) " );
                        break;
                    }
                case 5:
                    {
                        idealsList.Add( "Sincerity: There’s no good in pretending to be something I’m not. (Neutral) " );
                        break;
                    }
                case 6:
                    {
                        idealsList.Add( "Destiny: Nothing and no one can steer me away from my higher calling. (Any) " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID IDEAL SELECTED" );
                        break;
                    }
            }  // END OF SET CHARACTER IDEALS


            // SET CHARACTER BONDS
            roll = diceRoll( 1, 6 ); 
            switch ( roll )
            {
                case 1:
                    {
                        bondsList.Add( "I have a family, but I have no idea where they are. One day, I hope to see them again. " );
                        break;
                    }
                case 2:
                    {
                        bondsList.Add( "I worked the land, I love the land, and I will protect the land. " );
                        break;
                    }
                case 3:
                    {
                        bondsList.Add( "A proud noble once gave me a horrible beating, and I will take my revenge on any bully I encounter. " );
                        break;
                    }
                case 4:
                    {
                        bondsList.Add( "My tools are symbols of my past life, and I carry them so that I will never forget my roots. " );
                        break;
                    }
                case 5:
                    {
                        bondsList.Add( "I protect those who cannot protect themselves. " );
                        break;
                    }
                case 6:
                    {
                        bondsList.Add( "I wish my childhood sweetheart had come with me to pursue my destiny. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID BOND SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER BONDS


            // SET CHARACTER FLAWS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        flawsList.Add( "The tyrant who rules my land will stop at nothing to see me killed. " );
                        break;
                    }
                case 2:
                    {
                        flawsList.Add( "I’m convinced of the significance of my destiny, and blind to my shortcomings and the risk of failure. " );
                        break;
                    }
                case 3:
                    {
                        flawsList.Add( "The people who knew me when I was young know my shameful secret, so I can never go home again. " );
                        break;
                    }
                case 4:
                    {
                        flawsList.Add( "I have a weakness for the vices of the city, especially hard drink. " );
                        break;
                    }
                case 5:
                    {
                        flawsList.Add( "Secretly, I believe that things would be better if I were a tyrant lording over the land. " );
                        break;
                    }
                case 6:
                    {
                        flawsList.Add( "I have trouble trusting in my allies. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID FLAW SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER FLAWS
        }   // END OF createFolkHero


        public void createNoble()
        {
            characterBackground = "Noble";

            // ADD THE NOBLE SKILL PROFICIENCIES: History, Persuasion
            skillsArray[ (int) skillType.history ] += proficiency;
            skillsArray[ (int) skillType.persuasion ] += proficiency; 

            // ADD A GAMBLING PROFESSION
            selectGamblingProficiency();

            // NOBLES LEARN TO SPEAK, READ AND WRITE ONE EXTRA LANGUAGE OF THEIR CHOICE
            addExtraLanguage( 1 );

            equipmentList.Add( "set of fine clothes, signet ring, scroll of pedigree, purse, " );

            goldPieces = goldPieces + 25;   // NOBLES START WITH 25 gp

            // SET PERSONALITY TRAITS
            roll = diceRoll( 1, 8 );
            switch ( roll )
            {
                case 1:
                    { 
                        personalityTraitList.Add( "My eloquent flattery makes everyone I talk to feel like the most wonderful and important person in the world. " );
                        break;
                    }
                case 2:
                    {
                        personalityTraitList.Add( "The common folk love me for my kindness and generosity. " );
                        break;
                    }
                case 3:
                    {
                        personalityTraitList.Add( "No one could doubt by looking at my regal bearing that I am a cut above the unwashed masses. " );
                        break;
                    }
                case 4:
                    {
                        personalityTraitList.Add( "I take great pains to always look my best and follow the latest fashions. " );
                        break;
                    }
                case 5:
                    {
                        personalityTraitList.Add( "I don’t like to get my hands dirty, and I won’t be caught dead in unsuitable accommodations. " );
                        break;
                    }
                case 6: 
                    {
                        personalityTraitList.Add( "Despite my noble birth, I do not place myself above other folk. We all have the same blood. " );
                        break;
                    } 
                case 7: 
                    {
                        personalityTraitList.Add( "My favor, once lost, is lost forever. " );
                        break;
                    }
                case 8:
                    {
                        personalityTraitList.Add( "If you do me an injury, I will crush you, ruin your name, and salt your fields. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID TRAIT SELECTED" );
                        break;
                    }
            }  // END OF SET PERSONALITY TRAITS


            // SET CHARACTERS IDEALS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        idealsList.Add( "Respect: Respect is due to me because of my position, but all people regardless of station deserve to be treated with dignity. (Good) " );
                        break;
                    }
                case 2:
                    {
                        idealsList.Add( "Responsibility: It is my duty to respect the authority of those above me, just as those below me must respect mine. (Lawful) " );
                        break;
                    }
                case 3:
                    {
                        idealsList.Add( "Independence: I must prove that I can handle myself without the coddling of my family. (Chaotic) " );
                        break;
                    }
                case 4:
                    {
                        idealsList.Add( "Power: If I can attain more power, no one will tell me what to do. (Evil) " );
                        break;
                    }
                case 5:
                    {
                        idealsList.Add( "Family: Blood runs thicker than water. (Any) " );
                        break;
                    }
                case 6:
                    {
                        idealsList.Add( "Noble Obligation: It is my duty to protect and care for the people beneath me. (Good) " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID IDEAL SELECTED" );
                        break;
                    }
            }  // END OF SET CHARACTER IDEALS


            // SET CHARACTER BONDS
            roll = diceRoll( 1, 6 ); 
            switch ( roll )
            {
                case 1:
                    {
                        bondsList.Add( "I will face any challenge to win the approval of my family. " );
                        break;
                    }
                case 2:
                    {
                        bondsList.Add( "My house’s alliance with another noble family must be sustained at all costs. " );
                        break;
                    }
                case 3:
                    {
                        bondsList.Add( "Nothing is more important than the other members of my family. " );
                        break;
                    }
                case 4:
                    {
                        bondsList.Add( "I am in love with the heir of a family that my family despises. " );
                        break;
                    }
                case 5:
                    {
                        bondsList.Add( "My loyalty to my sovereign is unwavering. " );
                        break;
                    }
                case 6:
                    {
                        bondsList.Add( "The common folk must see me as a hero of the people. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID BOND SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER BONDS

           
            // SET CHARACTER FLAWS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        flawsList.Add( "I secretly believe that everyone is beneath me. " );
                        break;
                    }
                case 2:
                    {
                        flawsList.Add( "I hide a truly scandalous secret that could ruin my family forever. " );
                        break;
                    }
                case 3:
                    {
                        flawsList.Add( "I too often hear veiled insults and threats in every word addressed to me, and I’m quick to anger. " );
                        break;
                    }
                case 4:
                    {
                        flawsList.Add( "I have an insatiable desire for carnal pleasures. " );
                        break;
                    }
                case 5:
                    {
                        flawsList.Add( " In fact, the world does revolve around me. " );
                        break;
                    }
                case 6:
                    {
                        flawsList.Add( "By my words and actions, I often bring shame to my family. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID FLAW SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER FLAWS
        }   // END OF createNoble


        public void createSage()
        {
            characterBackground = "Sage";

            // ADD THE SAGE'S SKILL PROFICIENCIES: Arcana, History    
            skillsArray[ (int) skillType.arcana ]  += proficiency;
            skillsArray[ (int) skillType.history ] += proficiency;
            
            // SAGES LEARN TO SPEAK, READ AND WRITE TWO EXTRA LANGUAGE OF THEIR CHOICE
            addExtraLanguage( 2 );

            equipmentList.Add( "bottle of black ink, quill, small knife, letter from a dead colleague posing a question you have not yet been able to answer, set of common clothes, pouch, " );

            goldPieces = goldPieces + 10; // SAGES START WITH 10 gp

                
            // SET PERSONALITY TRAITS
            roll = diceRoll( 1, 8 );
            switch ( roll )
            {
                case 1:
                    {
                        personalityTraitList.Add( "I use polysyllabic words that convey the impression of great erudition. " );
                        break;
                    }
                case 2:
                    {
                        personalityTraitList.Add( "I’ve read every book in the world’s greatest libraries — or I like to boast that I have. " );
                        break;
                    }
                case 3:
                    {
                        personalityTraitList.Add( "I’m used to helping out those who aren’t as smart as I am, and I patiently explain anything and everything to others. " );
                        break;
                    }
                case 4:
                    {
                        personalityTraitList.Add( "There’s nothing I like more than a good mystery. " );
                        break;
                    }
                case 5:
                    {
                        personalityTraitList.Add( "I’m willing to listen to every side of an argument before I make my own judgment. " );
                        break;
                    }
                case 6: 
                    {
                        personalityTraitList.Add( "I. . .speak. . .slowly. . .when talking. . .to idiots, . . .which. . .almost. . .everyone. . . is . . . compared. . . to me. " );
                        break;
                    } 
                case 7: 
                    {
                        personalityTraitList.Add( "I am horribly, horribly awkward in social situations. " );
                        break;
                    }
                case 8:
                    {
                        personalityTraitList.Add( "I’m convinced that people are always trying to steal my secrets. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID TRAIT SELECTED" );
                        break;
                    }
            }  // END OF SET PERSONALITY TRAITS


            // SET CHARACTERS IDEALS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        idealsList.Add( "Knowledge: The path to power and self-improvement is through knowledge. (Neutral) " );
                        break;
                    }
                case 2:
                    {
                        idealsList.Add( "Beauty: What is beautiful points us beyond itself toward what is true. (Good) " );
                        break;
                    }
                case 3:
                    {
                        idealsList.Add( "Logic: Emotions must not cloud our logical thinking. (Lawful) " );
                        break;
                    }
                case 4:
                    {
                        idealsList.Add( "No Limits: Nothing should fetter the infinite possibility inherent in all existence. (Chaotic) " );
                        break;
                    }
                case 5:
                    {
                        idealsList.Add( "Power: Knowledge is the path to power and domination. (Evil) " );
                        break;
                    }
                case 6:
                    {
                        idealsList.Add( "Self - Improvement: The goal of a life of study is the betterment of oneself. (Any) " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID IDEAL SELECTED" );
                        break;
                    }
            }  // END OF SET CHARACTER IDEALS


            // SET CHARACTER BONDS
            roll = diceRoll( 1, 6 ); 
            switch ( roll )
            {
                case 1:
                    {
                        bondsList.Add( "It is my duty to protect my students. " );
                        break;
                    }
                case 2:
                    {
                        bondsList.Add( "I have an ancient text that holds terrible secrets that must not fall into the wrong hands. " );
                        break;
                    }
                case 3:
                    {
                        bondsList.Add( "I work to preserve a library, university, scriptorium, or monastery. " );
                        break;
                    }
                case 4:
                    {
                        bondsList.Add( "My life’s work is a series of tomes related to a specific field of lore. " );
                        break;
                    }
                case 5:
                    {
                        bondsList.Add( "I’ve been searching my whole life for the answer to a certain question. " );
                        break;
                    }
                case 6:
                    {
                        bondsList.Add( "I sold my soul for knowledge. I hope to do great deeds and win it back. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID BOND SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER BONDS


           // SET CHARACTER FLAWS
           roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        flawsList.Add( "I am easily distracted by the promise of information. " );
                        break;
                    }
                case 2:
                    {
                        flawsList.Add( "Most people scream and run when they see a demon.I stop and take notes on its anatomy. " );
                        break;
                    }
                case 3:
                    {
                        flawsList.Add( "Unlocking an ancient mystery is worth the price of a civilization. " );
                        break;
                    }
                case 4:
                    {
                        flawsList.Add( "I overlook obvious solutions in favor of complicated ones. " );
                        break;
                    }
                case 5:
                    {
                        flawsList.Add( "I speak without really thinking through my words, invariably insulting others. " );
                        break;
                    }
                case 6:
                    {
                        flawsList.Add( "I can’t keep a secret to save my life, or anyone else’s. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID FLAW SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER FLAWS
        }   // END OF createSage


        public void createSoldier()
        {
            // ADD THE SOLDIER SKILL PROFICIENCIES: Athletics, Intimidation
            skillsArray[ (int) skillType.athletics ]    += proficiency;
            skillsArray[ (int) skillType.intimidation ] += proficiency;

            // ADD A GAMBLING PROFESSION
            selectGamblingProficiency();

            proficienciesList.Add( "vehicles(land), " );
            equipmentList.Add( "insignia of rank, set of common clothes, pouch, " );

            // SELECT A TROPHY TAKEN FROM A FALLEN ENEMY, EITHER a dagger, broken blade, or piece of a banner
            roll = diceRoll( 1, 3 );
            switch ( roll )
            {
                case 1:
                    {
                        equipmentList.Add( "Dagger, " );
                        break;
                    }
                case 2:
                    {
                        equipmentList.Add( "Broken Blade, " );
                        break;
                    }
                case 3:
                    {
                        equipmentList.Add( "Piece of a Banner, " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID ITEM SELECTED" );
                        break;
                    }
            }

            // TODO ############### CHECK THAT PLAYERS DON'T ALREADY HAVE A SET OF CARDS OR DICE ##############################################################################################
            // SELECT A set of Bone Dice or Deck of Cards
            roll = diceRoll( 1, 2 );
            switch ( roll )
            {
                case 1:
                    {
                        equipmentList.Add( "Bone Dice, " );
                        break;
                    }
                case 2:
                    {
                        equipmentList.Add( "Deck of Cards, " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID ITEM SELECTED" );
                        break;
                    }
            }

            goldPieces = goldPieces + 10;   // SOLDIERS START WITH 10 gp

            // SET PLAYERS RANK WITHIN THE MILITARY (USED LATER TO DEFER TO OTHER CHARACTERS OF LOWER OR HIGHER RANK)
            roll = diceRoll( 1, 8 );
            switch ( roll )
            {
                case 1:
                    {
                        characterBackground = "Soldier ( Officer )";
                        break;
                    }
                case 2:
                    {
                        characterBackground = "Soldier ( Scout )";
                        break;
                    }
                case 3:
                    {
                        characterBackground = "Soldier ( Infantry )";
                        break;
                    }
                case 4:
                    {
                        characterBackground = "Soldier ( Cavalry )";
                        break;
                    }
                case 5:
                    {
                        characterBackground = "Soldier ( Healer )";
                        break;
                    }
                case 6:
                    {
                        characterBackground = "Soldier ( Quartermaster )";
                        break;
                    }
                case 7:
                    {
                        characterBackground = "Soldier ( Standard bearer )";
                        break;
                    }
                case 8:
                    {
                        characterBackground = "Soldier ( Support staff )";
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID RANK SELECTED" );
                        break;
                    }
            }  // END OF SET PLAYERS RANK 


            // SET PERSONALITY TRAITS
            roll = diceRoll( 1, 8 );
            switch ( roll )
            {
                case 1:
                    {
                        personalityTraitList.Add( "I’m always polite and respectful. " );
                        break;
                    }
                case 2:
                    {
                        personalityTraitList.Add( "I’m haunted by memories of war. I can’t get the images of violence out of my mind. " );
                        break;
                    }
                case 3:
                    {
                        personalityTraitList.Add( "I’ve lost too many friends, and I’m slow to make new ones. " );
                        break;
                    }
                case 4:
                    {
                        personalityTraitList.Add( "I’m full of inspiring and cautionary tales from my military experience relevant to almost every combat situation. " );
                        break;
                    }
                case 5:
                    {
                        personalityTraitList.Add( "I can stare down a hell hound without flinching. " );
                        break;
                    }
                case 6: 
                    {
                        personalityTraitList.Add( "I enjoy being strong and like breaking things. " );
                        break;
                    } 
                case 7: 
                    {
                        personalityTraitList.Add( "I have a crude sense of humor. " );
                        break;
                    }
                case 8:
                    {
                        personalityTraitList.Add( "I face problems head-on.A simple, direct solution is the best path to success. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID TRAIT SELECTED" );
                        break;
                    }
            }  // END OF SET PERSONALITY TRAITS


            // SET CHARACTERS IDEALS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        idealsList.Add( "Greater Good: Our lot is to lay down our lives in defense of others. (Good) " );
                        break;
                    }
                case 2:
                    {
                        idealsList.Add( "Responsibility: I do what I must and obey just authority. (Lawful) " );
                        break;
                    }
                case 3:
                    {
                        idealsList.Add( "Independence: When people follow orders blindly, they embrace a kind of tyranny. (Chaotic) " );
                        break;
                    }
                case 4:
                    {
                        idealsList.Add( "Might: In life as in war, the stronger force wins. (Evil) " );
                        break;
                    }
                case 5:
                    {
                        idealsList.Add( "Live and Let Live: Ideals aren’t worth killing over or going to war for. (Neutral)" );
                        break;
                    }
                case 6:
                    {
                        idealsList.Add( "Nation: My city, nation, or people are all that matter. (Any)" );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID IDEAL SELECTED" );
                        break;
                    }
            }  // END OF SET CHARACTER IDEALS


            // SET CHARACTER BONDS
            roll = diceRoll( 1, 6 ); 
            switch ( roll )
            {
                case 1:
                    {
                        bondsList.Add( "I would still lay down my life for the people I served with. " );
                        break;
                    }
                case 2:
                    {
                        bondsList.Add( "Someone saved my life on the battlefield.To this day, I will never leave a friend behind. " );
                        break;
                    }
                case 3:
                    {
                        bondsList.Add( "My honor is my life. " );
                        break;
                    }
                case 4:
                    {
                        bondsList.Add( "I’ll never forget the crushing defeat my company suffered or the enemies who dealt it. " );
                        break;
                    }
                case 5:
                    {
                        bondsList.Add( "Those who fight beside me are those worth dying for. " );
                        break;
                    }
                case 6:
                    {
                        bondsList.Add( "I fight for those who cannot fight for themselves. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID BOND SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER BONDS


            // SET CHARACTER FLAWS
            roll = diceRoll( 1, 6 );
            switch ( roll )
            {
                case 1:
                    {
                        flawsList.Add( "The monstrous enemy we faced in battle still leaves me quivering with fear. " );
                        break;
                    }
                case 2:
                    {
                        flawsList.Add( "I have little respect for anyone who is not a proven warrior. " );
                        break;
                    }
                case 3:
                    {
                        flawsList.Add( "I made a terrible mistake in battle that cost many lives and I would do anything to keep that mistake secret. " );
                        break;
                    }
                case 4:
                    {
                        flawsList.Add( "My hatred of my enemies is blind and unreasoning. " );
                        break;
                    }
                case 5:
                    {
                        flawsList.Add( "I obey the law, even if the law causes misery. " );
                        break;
                    }
                case 6:
                    {
                        flawsList.Add( "I’d rather eat my armor than admit when I’m wrong. " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID FLAW SELECTED" );
                        break;
                    }
            }   // END OF SET CHARACTER FLAWS
        }   // END OF createSoldier


        // ### 5 - EQUIPMENT METHODS ###
        private void addBurglarsPack()
        {
            equipmentList.Add( "backpack, bag of 1,000 ball bearings, 10 feet of string, bell, 5 candles, crowbar, hammer, 10 pitons, hooded lantern, 2 flasks of oil, 5 days of rations, tinderbox, waterskin, 50 feet of hempen rope, " );
        }

        private void addDiplomatsPack()
        {
            equipmentList.Add( "chest, 2 cases for maps and scrolls, set of fine clothes, bottle of ink, ink pen, lamp, 2 flasks of oil, 5 sheets of paper, a vial of perfume, sealing wax, soap, " );
        }

        private void addDungeoneersPack()
        {
            equipmentList.Add( "backpack, crowbar, hammer, 10 pitons, 10 torches, tinderbox, 10 days of rations, waterskin, 50 feet of hempen rope, " );
        }

        private void addEntertainersPack()
        {
            equipmentList.Add( "backpack, bedroll, 2 costumes, 5 candles, 5 days of rations, waterskin, disguise kit, " );
        }

        private void addExplorersPack()
        {
            equipmentList.Add( "backpack, a bedroll, mess kit, tinderbox, 10 torches, 10 days of rations, waterskin, 50 feet of hempen rope, " );
        }

        private void addPriestsPack()
        {
            equipmentList.Add( "backpack, blanket, 10 candles, tinderbox, alms box, 2 blocks of incense, censer, vestments, 2 days of rations, waterskin, " );
        }

        private void addScholarsPack()
        {
            equipmentList.Add( "backpack, book of lore, bottle of ink, ink pen, 10 sheets of parchment, little bag of sand, small knife, " );
        }


        private void selectGamblingProficiency()
        {
            // SELECT GAMBLING PROFICIENCY
            roll = diceRoll( 1, 4 );
            switch ( roll )
            {
                case 1:
                    {
                        proficienciesList.Add( "Dice, " );
                        break;
                    }
                case 2:
                    {
                        proficienciesList.Add( "Dragonchess, " );
                        break;
                    }
                case 3:
                    {
                        proficienciesList.Add( "Playing cards, " );
                        break;
                    }
                case 4:
                    {
                        proficienciesList.Add( "Three - Dragon Ante, " );
                        break;
                    }
                default:
                    {
                        invalidInput( "ERROR INVALID GAMBLING PROFICIENCY SELECTED" );
                        break;
                    }
            }   
        }   // END OF selectGamblingProficiency


        // ####################### UTILITY FUNCTIONS ##############################################

        public void invalidInput(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
            Environment.Exit(0);
        }

        // CREATED TO ALLOW RANDOM DICE ROLLS ( THIS FUNCTION WAS ADDED, AS UNITY AND C# USE DIFFERENT RANDOM FUNCTIONS )
        private int diceRoll( int min, int max )
        {
            return dice.Next( min, max );
        }




        private void printToConsole()
        {
            Console.WriteLine(" ### PRINT TO CONSOLE ###");
            Console.WriteLine("Character Name : " + characterName + "\t Player Name : _0_______________________");
            Console.WriteLine("Class : " + characterClass + "\t Level : " + characterLevel + "\t Background : " + characterBackground);
            Console.WriteLine("Race : " + race + "\t Alignment : " + alignment + "\t Exp Points : _0_________________");
            Console.WriteLine("");
            Console.WriteLine(" Stats & : Strength   " + "Dexterity  " + "Constitution  " + "Intelligence  " + "Wisdom  " + "Charisma");
            Console.WriteLine(" Modifier: " + strength + " (" + intToString(strengthModifier) + " )  " + dexterity + " (" + intToString(dexterityModifier) + " )  " + constitution + " (" + intToString(constitutionModifier) + " )     " + intelligence + " (" + intToString(intelligenceModifier) + " )    " + wisdom + " (" + intToString(wisdomModifier) + " ) " + charisma + " (" + intToString(charismaModifier) + " )");
            Console.WriteLine("Insperation : " + inspiration + "\t Proficiency Bonus : " + proficiency);
            Console.WriteLine("Saving Throws - Str : " + intToString(strengthSavingThrow) + "   Dex : " + intToString(dexteritySavingThrow) + "   Cons : " + intToString(constitutionSavingThrow) + "   Intell : " + intToString(intelligenceSavingThrow) + "   Wis : " + intToString(wisdomSavingThrow) + "   Chr : " + intToString(charismaSavingThrow) );
            Console.WriteLine("");
            //Console.WriteLine("SKILLS : Acrobatics etc");

            Console.WriteLine( "Acrobatics :     "      + intToString(skillsArray [(int)skillType.acrobatics]) );
            Console.WriteLine( "Animal Handling : " + intToString(skillsArray [(int)skillType.animalHandling]) );
            Console.WriteLine( "Arcana :     \t"          + intToString(skillsArray [(int)skillType.arcana]) );
            Console.WriteLine( "Athletics :     "       + intToString(skillsArray [(int)skillType.athletics]) );
            Console.WriteLine( "Deception :     "       + intToString(skillsArray [(int)skillType.deception]) );
            Console.WriteLine( "History :     \t"         + intToString(skillsArray [(int)skillType.history]) );
            Console.WriteLine( "Insight :     \t"         + intToString(skillsArray [(int)skillType.insight]) );
            Console.WriteLine( "Intimidation :  "    + intToString(skillsArray [(int)skillType.intimidation]) );
            Console.WriteLine( "Investigation : "   + intToString(skillsArray [(int)skillType.investigation]) );
            Console.WriteLine( "Medicine :     \t"        + intToString(skillsArray [(int)skillType.medicine]) );
            Console.WriteLine( "Nature :     \t"          + intToString(skillsArray [(int)skillType.nature]) );
            Console.WriteLine( "Perception :   \t"      + intToString(skillsArray [(int)skillType.perception]) );
            Console.WriteLine( "Performance :   "     + intToString(skillsArray [(int)skillType.performance]) );
            Console.WriteLine( "Persuasion :   \t"      + intToString(skillsArray [(int)skillType.persuasion]) );
            Console.WriteLine( "Religion :     \t"        + intToString(skillsArray [(int)skillType.religion]) );
            Console.WriteLine( "Sleight of Hand : " + intToString(skillsArray [(int)skillType.sleightOfHand]) );
            Console.WriteLine( "Stealth :     \t"         + intToString(skillsArray [(int)skillType.stealth]) );
            Console.WriteLine( "Survival :     \t"        + intToString(skillsArray [(int)skillType.survival]) );
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("Passive Wisdom : " + passiveWisdom + "\t Armor Class : " + armorClass);
            Console.WriteLine("initiative : " + initiative + "\t Speed : " + characterSpeed + "\t Hit Points : " + hitPointsMax);
            Console.WriteLine("Hit Dice : " + hitDice);
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("OTHER PROFICIENCES / LANGUAGES");
            for (int i = 0; i < languageList.Count; i++)
            {
                string output = languageList [i] as string;
                languagesAsString = languagesAsString + output;
            }
            Console.WriteLine("Languages: " + languagesAsString);

            for (int i = 0; i < proficienciesList.Count; i++)
            {
                string output = proficienciesList [i] as string;
                Console.Write(output);
            }
            Console.WriteLine("\n-------------------------------------------------------------------------------");
            Console.WriteLine("");
            Console.WriteLine("Attacks / Spellcasting");
            Console.WriteLine("");
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("EQUIPMENT ");
            Console.WriteLine("CP : " + copperPieces + "\t SP : " + silverPieces + "\t EP : " + electrumPieces + "\t GP : " + goldPieces + "\t PP : " + platinumPieces);
            for (int i = 0; i < equipmentList.Count; i++)
            {
                string output = equipmentList [i] as string;
                Console.Write(output);
            }
            Console.WriteLine("\n-------------------------------------------------------------------------------");
            Console.WriteLine("Personality Traits");
            for (int i = 0; i < personalityTraitList.Count; i++)
            {
                Console.WriteLine(personalityTraitList [i]);
            }
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("Ideals");
            for (int i = 0; i < idealsList.Count; i++)
            {
                Console.WriteLine(idealsList [i]);
            }
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("Bonds");
            for (int i = 0; i < bondsList.Count; i++)
            {
                Console.WriteLine(bondsList [i]);
            }
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("Flaws");
            for (int i = 0; i < flawsList.Count; i++)
            {
                Console.WriteLine(flawsList [i]);
            }
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("Features / Traits");
            for (int i = 0; i < featureTraitList.Count; i++)
            {
                Console.WriteLine(featureTraitList [i]);
            }
            Console.WriteLine("-------------------------------------------------------------------------------");

            Console.ReadLine();
        }

/*      ### FUNTIONS IN THE UNITY SCRIPT, THAT MAY BE NEEDED... ###

        // CONVERT EACH ELEMENT OF THE ARRAY LISTS TO A FORMATTED STRING TO BE ADDED TO THE RELEVEANT SECTION WITHIN UNITY
        private string arrayListToStringFormatted( ArrayList listToConvert )

        // CONVERT EACH ELEMENT OF THE ARRAY LISTS TO A CONTINUOUS STRING TO BE ADDED TO THE RELEVEANT SECTION WITHIN UNITY
        private string arrayListToString( ArrayList listToConvert )
*/
        // CONVERT THE INTEGER TO A STRING, AND ADD THE POSITIVE, OR NEGATIVE TO THE RESULTANT STRING
        private string intToString( int convertThis )
        {
            if ( convertThis <= 0 )      
            {
                return convertThis.ToString();  // RESULTANT STRING SHOULD BE RETURNED AS NORMAL
            }
            else if ( convertThis > 0 )
            {
                return ( "+" + convertThis.ToString() );  // RESULTANT STRING SHOULD HAVE A POSITIVE SIGN ADDED AS A PREFIX BEFORE RETURNING
            }
            return "ERROR";
        }


        private void clearVariables ()
        {
            // ########################################################################################################################
            // WHEN A NEW CHARACTER IS GENERATED, ALMOST ALL VARAIBLES SIMPLY ASSIGN A NEW VALUE OVERTOP OF THE OLD, SO THEY CAN BE LEFT ALONE
            // HOWEVER SOME, SUCH AS ARRAY LISTS, SIMPLY GET "ADDED TO", WHICH IS WHY THEY HAVE TO GET RESET MANUALLY BEFORE A NEW CHARACTER IS GENERATED
            // ########################################################################################################################

            strengthSavingThrow     = 0;
            dexteritySavingThrow    = 0;
            constitutionSavingThrow = 0;
            intelligenceSavingThrow = 0;
            wisdomSavingThrow       = 0;
            charismaSavingThrow     = 0;

            // INITIALIZE THE skillsArray TO ZERO 
            for ( int i = 0; i < skillsArray.Length; i++ )
            {
                skillsArray[ i ] = 0;
            }

            // CLEAR EACH OF THE ARRAY LISTS
            proficienciesList.Clear();
            languageList.Clear();    

            personalityTraitList.Clear();
            idealsList.Clear();  
            bondsList.Clear();    
            flawsList.Clear();
            featureTraitList.Clear();

            // 5 - EQUIPMENT
            copperPieces    = 0;
            silverPieces    = 0;
            electrumPieces  = 0;
            goldPieces      = 0;
            platinumPieces  = 0;
            equipmentList.Clear();      
        }


        // THIS FUNCTION IS USED TO ASSIGN ANY EXTRA LANGUAGES PLAYERS ARE REQUIRED TO CHOOSE, AND CAN BE REPEATED, USING THE ARGUMENT
        private void addExtraLanguage( int noOfLanguagesToAdd )
        {
            for ( int i = 0; i < noOfLanguagesToAdd; i++ )
            {
                // ALL CHARACTER TYPES KNOW THE COMMON TONGUE, SO IT WILL NOT BE INCLUDED WHEN "ADDING" AN EXTRA LANGUAGE
                languageList.Add ( allLanguages[ diceRoll( 2, allLanguages.Length - 1 ) ] + ", " );
            }
        }


        private void chooseAlignment()
        {
            roll = diceRoll( 1, 9 );
            switch ( roll )
            {
                case 1:
                    {
                        alignment = "Lawful Good ";
                        break;
                    }
                case 2:
                    {
                        alignment = "Neutral Good ";
                        break;
                    }
                case 3:
                    {
                        alignment = "Chaotic Good ";
                        break;
                    }
                case 4:
                    {
                        alignment = "Lawful Neutral ";
                        break;
                    }
                case 5:
                    {
                        alignment = "Neutral ";
                        break;
                    }
                case 6: 
                    {
                        alignment = "Chaotic Neutral ";
                        break;
                    } 
                case 7: 
                    {
                        alignment = "Lawful Evil ";
                        break;
                    }
                case 8:
                    {
                        alignment = "Neutral Evil ";
                        break;
                    }
                case 9:
                    {
                        alignment = "Chaotic Evil ";
                        break;
                    }  
                default:
                    {
                        invalidInput( "ERROR INVALID ALIGNMENT SELECTED" );
                        break;
                    }
            } // END SWITCH
        } // END private void chooseAlignment()

    }  // CLOSE public class Character

}  // CLOSE namespace CCS_D_D_Character_Generator
